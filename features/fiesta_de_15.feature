#language: es
@wip
Característica: Presupuestación de fiesta de 15

Antecedentes:
  Dado que mi cuit personal es "20445556667"

  Escenario: f1 - Fiesta de 15 discreta con menu veggie
    Dado un evento "fiesta de 15" con servicio "normal"
    Y que está programado para el "2021-05-11" que es un día hábil
    Y que tendrá 1 comensales con menú "veggie"
    Cuando se presupuesta
    Entonces el importe resultante es 350.0

  Escenario: f2 - Fiesta de 15 grande con tres menues
    Dado un evento "fiesta de 15" con servicio "normal"
    Y que está programado para el "2021-05-11" que es un día hábil
    Y que tendrá 30 comensales con menú "veggie"
    Y que tendrá 10 comensales con menú "carnie"
    Y que tendrá 50 comensales con menú "mixto"
    Cuando se presupuesta
    Entonces el importe resultante es 14450.0 

  Escenario: f3 - Actualizacion de la cantidad de comensales a 2 a Fiesta de 15 discreta con menu veggie
    Dado un evento "fiesta de 15" con servicio "normal"
    Y que está programado para el "2021-05-11" que es un día hábil
    Y que tendrá 2 comensales con menú "veggie"
    Cuando se presupuesta
    Entonces el importe resultante es 500.0

  Escenario: f4 - Fiesta de 15 ampulosa con tres menus
    Dado un evento "fiesta de 15" con servicio "premium"
    Y que está programado para el "2021-05-11" que es un día hábil
    Y que tendrá 50 comensales con menú "mixto"
    Y que tendrá 50 comensales con menú "veggie"
    Y que tendrá 50 comensales con menú "carnie"
    Cuando se presupuesta
    Entonces el importe resultante es 26250.0

  Escenario: f5 - Fiesta de 15 ampulosa con tres menus en día no hábil
    Dado un evento "fiesta de 15" con servicio "premium"
    Y que está programado para el "2021-05-16" que es un día no hábil
    Y que tendrá 50 comensales con menú "mixto"
    Y que tendrá 50 comensales con menú "veggie"
    Y que tendrá 50 comensales con menú "carnie"
    Cuando se presupuesta
    Entonces el importe resultante es 28875.0
