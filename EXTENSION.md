Extension
=========

A partir del entusiasmo inicial despertado por la aplicación de presupuestos se decidió agregar las siguientes funcionalidades:

* Descuento de jueves: aquellos presupuestos que sean generados los días jueves contarán con un descuento de 5%

* Fiestas de graduación: tienen un costo base 120, siempre incluyen salón y tienen una restricción de comensales entre 20 y 60. No pueden ser contratadas por empresas. Y tienen un costo extra de 'seguro por destrozos' de 50 pesos por comensal

* Inclusión de brindis: en el caso de los casamientos es posible también contratar el servicio de brindis el cual tiene un costo variable dependiendo de la cantidad de comensales. Hasta 50 comensales cuesta 10 por comensal y por encima de 50 cuesta 8 comensal.


