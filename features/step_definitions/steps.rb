Dado("que mi cuit personal es {string}") do |cuit|
  @cuit = cuit
end

Dado("un evento {string} con servicio {string}") do |tipo_evento, nivel_servicio|
  @tipo_evento = tipo_evento
  @nivel_servicio = nivel_servicio
end

Dado("que está programado para el {string} que es un día hábil") do |fecha|
  @fecha = fecha
  salon = @salon || false
  body = {
    'cuit': @cuit,
    'tipo_evento': @tipo_evento,
    'nivel_servicio': @nivel_servicio,
    'fecha': @fecha,
    'salon': salon
  }
  post '/presupuestos', body.to_json, "CONTENT_TYPE" => "application/json"
  expect(last_response.status).to be == 201
  @id = JSON.parse(last_response.body)['id']
end

Cuando("intento programarlo para el {string}") do |fecha|
  @fecha = fecha
  salon = @salon || false
  body = {
    'cuit': @cuit,
    'tipo_evento': @tipo_evento,
    'nivel_servicio': @nivel_servicio,
    'fecha': @fecha,
    'salon': salon
  }
  post '/presupuestos', body.to_json, "CONTENT_TYPE" => "application/json"
end


Dado("que está programado para el {string} que es un día no hábil") do |fecha|
  @fecha = fecha
  salon = @salon || false
  body = {
      'cuit': @cuit,
      'tipo_evento': @tipo_evento,
      'nivel_servicio': @nivel_servicio,
      'fecha': @fecha,
      'salon': salon
  }
  post '/presupuestos', body.to_json, "CONTENT_TYPE" => "application/json"
  expect(last_response.status).to be == 201
  @id = JSON.parse(last_response.body)['id']
end


Dado("que tendrá {int} comensales con menú {string}") do |cantidad_comensales, menu|
  @cantidad_comenzales = cantidad_comensales
  @menu = menu
  body = {
      'cantidad_comensales': @cantidad_comenzales,
      'menu': @menu
  }
  patch "/presupuestos/#{@id}", body.to_json, "CONTENT_TYPE" => "application/json"
  expect(last_response.status).to be == 200
end

Cuando("se presupuesta") do
  get "/presupuestos/#{@id}"
end

Entonces("el importe resultante es {float}") do |importe|
  expect(last_response.status).to eq(200)
  data = JSON.parse(last_response.body)
  expect(data['importe']).to eq importe.to_f
end

Entonces("obtengo un error") do
  expect(last_response.status).to eq(400)
end

Dado("contrata un salon") do
  @salon = true
end