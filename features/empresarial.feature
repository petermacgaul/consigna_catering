#language: es
@wip
Característica: Presupuestación de empresarial

Antecedentes:
  Dado que mi cuit personal es "30445556667"

  Escenario: e1 - Empresarial con menu mixto y servicio premium en dia habil
    Dado un evento "empresarial" con servicio "premium"
    Y que está programado para el "2021-05-18" que es un día hábil
    Y que tendrá 30 comensales con menú "mixto"
    Cuando se presupuesta
    Entonces el importe resultante es 8250.0
    # COSTO_BASE (200) * CANTIDAD_DE_COMENZALES(30) + CANTIDAD_DE_MOZOS(3) * COSTO_MOZO (200) + MENU (55) * CANTIDAD_DE_COMENZALES(30)

  Escenario: e2 - Empresarial servicio superior con menu carnie en sabado
    Dado un evento "empresarial" con servicio "superior"
    Y que está programado para el "2021-05-08" que es un día no hábil
    Y que tendrá 50 comensales con menú "carnie"
    Cuando se presupuesta
    Entonces el importe resultante es 14740.0
    # COSTO_BASE (200) * CANTIDAD_DE_COMENZALES(50) + CANTIDAD_DE_MOZOS(2) * COSTO_MOZO (200) + MENU (60) * CANTIDAD_DE_COMENZALES(50) + 10% POR DIA NO HABIL

  Escenario: e3 - Empresarial servicio normal con menu mixto en dia habil
    Dado un evento "empresarial" con servicio "normal"
    Y que está programado para el "2021-04-08" que es un día hábil
    Y que tendrá 15 comensales con menú "carnie"
    Y que tendrá 15 comensales con menú "veggie"
    Cuando se presupuesta
    Entonces el importe resultante es 7850.0
    # COSTO_BASE (200) * CANTIDAD_DE_COMENZALES(30) + CANTIDAD_DE_MOZOS(1) * COSTO_MOZO (200) + MENU (50) * CANTIDAD_DE_COMENZALES(15) + MENU (60) * CANTIDAD_DE_COMENZALES(15) + DIA HABIL

  Escenario: e4 - Empresarial servicio normal con menu mixto en dia no habil
    Dado un evento "empresarial" con servicio "normal"
    Y que está programado para el "2021-05-08" que es un día no hábil
    Y que tendrá 45 comensales con menú "carnie"
    Y que tendrá 15 comensales con menú "mixto"
    Cuando se presupuesta
    Entonces el importe resultante es 17517.5
    # COSTO_BASE (200) * CANTIDAD_DE_COMENZALES(60) + CANTIDAD_DE_MOZOS(1) * COSTO_MOZO (200) + MENU (60) * CANTIDAD_DE_COMENZALES(45) + MENU (55) * CANTIDAD_DE_COMENZALES(15) + DIA NO HABIL

  Escenario: e5 - Persona fisica contrata evento empresarial
    Dado que mi cuit personal es "20445556667"
    Dado un evento "empresarial" con servicio "normal"
    Y que está programado para el "2021-05-11" que es un día hábil
    Y que tendrá 50 comensales con menú "veggie"
    Cuando se presupuesta
    Entonces el importe resultante es 12900.0